<?php
class ModelSyncSync extends Model {
    private $auth = "TS5ZQU5HOjhublN2QWllWDlOaDI3T3VIbnVGbk1ZbFJzcjJ0U1cvaE5aZitubk8xNUk9";
    private $endpoint = 'https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies';
    private $endpoint_1 = "https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(41456175-baaa-4c6a-bbee-15c57159eedf)/salesOrders?\$filter=number eq 'SO-000432'";
    private $endpoint_2 = "https://api.businesscentral.dynamics.com/v2.0/74611a07-434f-4aba-89c2-c71ef6e38813/Production/ODataV4/Company('Pilot%20Test%20Dec%203%20backup')/getSalesPrice?\$filter=Sales_Type eq 'Customer' and Sales_Code eq 'OPENCART'";

    private $item = "https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(41456175-baaa-4c6a-bbee-15c57159eedf)/items?\$filter=number eq '8806358586508'";

    private $order_url = 'https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(41456175-baaa-4c6a-bbee-15c57159eedf)/salesOrders';
    // (5cab940a-9472-ea11-a813-000d3a840bb2)/salesOrderLines

    public function Auth($url = false, $method = false, $data = false) {
        $accessToken = $this->session->data['accessToken'];

        if (!$method) {
            $method = 'GET';
        }

        $sales_order = array(
            'orderDate' => date('Y-m-d'),
            'customerNumber'    => 'OPENCART',
            'currencyCode'      => 'CAD',
            'paymentTermsId'    => 'f74bb0fb-2ee5-4fa3-85bd-e4598e2c3b77'
        );

        $sales_order_line = array(
            "itemId"        => "ed2c2454-9632-4773-be55-000103c04a6c",
            "lineType"      => "Item",
            "quantity"      => 9,
            "unitPrice"     => 2,
            "discountAmount"=> 0,
            "taxCode"       => 'TAXABLE',
            'reservedQuantity'   => 9
        );  

        try {
            // Create a Graph client
            $graph = new Microsoft\Graph\Graph();
            $graph->setAccessToken($accessToken['access_token']);

            try {
                
                if ($method == 'GET') {
                    $result = $graph->createRequest('GET', $url)->execute();
                } else {
                    // Test POST Request
                    $result = $graph->createRequest($method, $url)
                    ->attachBody($data)
                    ->execute();
                }

                // Test POST Request
                //$result = $graph->createRequest('POST', $this->order_url)
                //    ->attachBody($sales_order)
                //    ->execute();

                //$result = $graph->createRequest('GET', $url)->execute();
                //echo "<pre>"; print_r($result->getBody()); die("====");
                return $result->getBody();

            } catch (\Microsoft\Graph\Exception\GraphException $e) {
                
                // Failed to call api
                die($e->getMessage());
            }
        } catch (\League\OAuth2\Client\Provider\Exception\IdentityProviderException $e) {
        
            // Failed to get the access token
            die($e->getMessage());
        
        }
    }
}
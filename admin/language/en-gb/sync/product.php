<?php
// Heading
$_['heading_title']          = 'Sync Items';

// Text
$_['text_list']              = 'Sync';
$_['text_result']            = 'Sync Results';
$_['text_success']           = 'Sync Success';

// Button
$_['button_sync_category']   = 'Sync Item Category'; 
$_['button_sync_item']       = 'Sync Item'; 
$_['button_signin']          = 'Sign In';

// Error
$_['error_permission']       = 'You do not have Permission';
$_['error_api']              = 'API Call Error';
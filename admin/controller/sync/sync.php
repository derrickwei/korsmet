<?php
class ControllerSyncSync extends Controller {
    private $clientId       = 'e4f58a80-1532-4e4c-a203-07fb3ea20e17';
    private $clientSecret   = '?A9HMSFfR7--WJ?ufXqiCguJ2d26gRl8';
    private $redirectUri    = 'http://localhost/demostore/index.php?route=common/home/callback';
    private $urlAuthorize   = 'https://login.windows.net/common/oauth2/authorize?resource=https://api.businesscentral.dynamics.com';
    private $urlAccessToken = 'https://login.windows.net/common/oauth2/token?resource=https://api.businesscentral.dynamics.com';

    public function signin() {//print_r($this->oauth_para['clientId']);die("===");
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId'                => $this->clientId,            // The client ID assigned to you by the provider
            'clientSecret'            => $this->clientSecret,        // The client password assigned to you by the provider
            'redirectUri'             => $this->redirectUri,
            'urlAuthorize'            => $this->urlAuthorize,
            'urlAccessToken'          => $this->urlAccessToken,
			'urlResourceOwnerDetails' => '',
        ]);

        $authUrl = $provider->getAuthorizationUrl();
        $this->session->data['oauthState'] = $provider->getState();

        header('Location: ' . $authUrl);
        exit;    
    }
}
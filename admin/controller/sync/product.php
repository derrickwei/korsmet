<?php
class ControllerSyncProduct extends Controller {
    private $error = array();
	private $endpoint_companies = 'https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies';
	
	// **** Development Environment **** //
	//private $endpoint_itemCategories = 'https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(41456175-baaa-4c6a-bbee-15c57159eedf)/itemCategories';
	//private $endpoint_items = "https://api.businesscentral.dynamics.com/v2.0/74611a07-434f-4aba-89c2-c71ef6e38813/Production/ODataV4/Company('Pilot%20Test%20Dec%203%20backup')/getSalesPrice?\$filter=Sales_Type eq 'Customer' and Sales_Code eq 'OPENCART'";
	//private $endpoint_itemsSwitch = "https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(79e3a081-7ac2-45f7-998d-3d1b2e925d2a)/items?\$filter=blocked eq false";
	//private $endpoint_item_details = "https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(41456175-baaa-4c6a-bbee-15c57159eedf)/items?\$filter=number eq ";

	// **** Production Environment **** //
	private $endpoint_itemCategories = 'https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(79e3a081-7ac2-45f7-998d-3d1b2e925d2a)/itemCategories';
	private $endpoint_items = "https://api.businesscentral.dynamics.com/v2.0/74611a07-434f-4aba-89c2-c71ef6e38813/Production/ODataV4/Company('Korsmet%20Inc.')/getSalesPrice?\$filter=Sales_Type eq 'Customer' and Sales_Code eq 'OPENCART'";
	private $endpoint_itemsSwitch = "https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(79e3a081-7ac2-45f7-998d-3d1b2e925d2a)/items?\$filter=blocked eq false";
	private $endpoint_item_details = "https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(79e3a081-7ac2-45f7-998d-3d1b2e925d2a)/items?\$filter=number eq ";

	private $endpoint_items_test = "https://api.businesscentral.dynamics.com/v1.0/api/v1.0/companies(79e3a081-7ac2-45f7-998d-3d1b2e925d2a)/items?\$filter=type eq 'Service'";

	public function index() {
		$this->load->language('sync/product');

		$this->document->setTitle($this->language->get('heading_title'));

		//$this->load->model('sync/category');

		$this->getList();
    }

    public function sync() {
        $this->load->model('sync/sync');
		$this->load->language('sync/product');
		
        $json = array();
        
		//$result = $this->model_sync_sync->Auth($this->request->get['type'] == 'item' ? $this->endpoint_items : $this->endpoint_itemCategories); 
		$result = $this->model_sync_sync->Auth($this->endpoint_items_test);
		echo "<pre>"; print_r($result); die("===");
		
		switch($this->request->get['type']) {
			case 'item':
				$items = array();

				if (empty($result['value'])) {
					$result = $this->model_sync_sync->Auth($this->endpoint_itemsSwitch);
					//echo "<pre>"; print_r($result); die("===");
					foreach ($result['value'] as $i) {
						$items[] = array(
							'Unit_of_Measure_Code'		=> '',
							'Unit_Price'				=> '',
							'Minimum_Quantity'			=> '',
							'Details'					=> $i
						);
					}//echo "<pre>"; print_r($items); die("===");
					
				} else {
					foreach ($result['value'] as $i) {
						$itemDetail = $this->model_sync_sync->Auth($this->endpoint_item_details . "'" . $i['Item_No'] . "'");
						//echo "<pre>"; print_r($itemDetail); die("123123123");
						$items[] = array(
							'Unit_of_Measure_Code'		=> $i['Unit_of_Measure_Code'],
							'Unit_Price'				=> $i['Unit_Price'],
							'Minimum_Quantity'			=> $i['Minimum_Quantity'],
							'Details'					=> $itemDetail['value'][0]
						);
					}
				} //echo "<pre>"; print_r($items); die("===");

				$this->load->model('catalog/product');

				foreach ($items as $p) { //echo "<pre>"; print_r($p); die("===");
					$product = array();

					// Set product default value
					$product['model']		= $p['Details']['id'];
					$product['sku'] 		= '';
					$product['upc'] 		= '';
					$product['ean'] 		= '';
					$product['jan'] 		= '';
					$product['isbn'] 		= '';
					$product['mpn'] 		= '';
					$product['location'] 	= '';
					$product['price']		= $p['Unit_Price'];
					$product['tax_class_id'] 	= 11;
					$product['quantity'] 		= $p['Details']['inventory'];
					$product['minimum'] 		= $p['Minimum_Quantity'];
					$product['subtract'] 		= 1;	
					$product['stock_status_id'] = 5;	
					$product['shipping']		= 1;	
					$product['date_available'] 	= date('Y-m-d');	
					$product['length'] 			= '';	
					$product['width'] 			= '';
					$product['height'] 			= '';
					$product['length_class_id'] = 1;
					$product['weight'] 			= '';
					$product['weight_class_id'] = '';				
					$product['status'] 			= 1;
					$product['sort_order']		= 1;
					$product['manufacturer']	= '';
					$product['manufacturer_id'] = 0;
					$product['category']		= ''; //print_r($p['Details']['itemCategoryCode']);die(";;;");
					//$product['product_category'] 	= array('0' => $this->model_catalog_product->getCategoryIdByName($p['Details']['itemCategoryCode']));
					$product['product_description'] = array(
						'1'	=> array(
							'name'			=> $p['Details']['displayName'],
							'description'	=> '',
							'meta_title'	=> $p['Details']['displayName'],
							'meta_description'	=> '',
							'meta_keyword'		=> '',
							'tag'				=> ''
						)
					);

					$this->model_catalog_product->addProduct($product);
				}

			break;

			case 'category':
				$this->load->model('catalog/category');

				foreach ($result['value'] as $ct) { 
					$category = array();

					// Set category default value
					$category['parent_id']  = 0;
					$category['column']		= 1;
					$category['sort_order'] = 0;
					$category['status']		= 1;
					
					// Set category description
					$category['category_description'] = array(
						'1' => array(
							'name'    		=> $ct['displayName'],
							'meta_title'	=> $ct['displayName'],
							'description'	=> $ct['displayName'],
							'meta_description' => '',
                    		'meta_keyword'     => ''
						)
					);

					// Add Category
					$this->model_catalog_category->addCategory($category);
				}
				
			break;

			default:
			break;
		}

        if (!$this->user->hasPermission('modify', 'sync/category')) {
            $json['error'] = $this->language->get('error_permission');
        } else if (!$result) {
            $json['error'] = $this->language->get('error_api');
        } else {
            $json['success'] = $this->language->get('text_success');
            $json['result'] = $result;
        }

        $this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}
    
    protected function getList() {
        $url = '';

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('sync/product', 'user_token=' . $this->session->data['user_token'] . $url, true)
		);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['signin'] = $this->url->link('sync/sync/signin', 'user_token=' . $this->session->data['user_token'] . $url, true);
        
        $data['user_token'] = $this->session->data['user_token'];

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('sync/product', $data));
	}
}